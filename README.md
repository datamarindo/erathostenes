# erathostenes

just a comparison of the good old Erathostenes sieve, with the improvement of relieving a bottle-neck where the `while` loop iterated over non-prime numbers as it increased at `i+1` pace; this algorithim discards non-prime numbers as they are detected, thus it leaps from 23 to 29, as 24, 25, 26, 27 and 28 would already be discarded.

`erathostenes_web` is a script from [Gene Dan](https://genedan.com/tag/eratosthenes-sieve-in-r/)

```
>    system.time(erathostenes_web(1000000))
   user  system elapsed 
  11.82    1.31   13.62 
> system.time(erathostenes_datamarindo(1000000))
   user  system elapsed 
   0.35    0.05    0.47
```

and a nice `cli` print was added, although not as remarkably fast as the algorithm, 
<img src="r_ux_erathostenes.PNG"  width="400" height="200">

# Julia
same principle was coded in Julia, but the speed was not good (at least on a windows machine with Julia.exe), calculation took around 6 s, and another version found on the internet was not better than 18 s.


# is it Euler's?
apparently, this describes better the functioning of this algorithm (from [Wikipedia](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes)):
>Euler's proof of the zeta product formula contains a version of the sieve of Eratosthenes in which each composite number is eliminated exactly once.[8] The same sieve was rediscovered and observed to take linear time by Gries & Misra (1978).[18] It, too, starts with a list of numbers from 2 to n in order. On each step the first element is identified as the next prime, is multiplied with each element of the list (thus starting with itself), and the results are marked in the list for subsequent deletion. The initial element and the marked elements are then removed from the working sequence, and the process is repeated.
